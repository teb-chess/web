import * as webpack from 'webpack';
import * as HtmlWebPackPlugin from 'html-webpack-plugin';
import * as path from 'path';

const htmlPlugin = new HtmlWebPackPlugin({
    template: './public/index.html',
    filename: './index.html',
});

const config: any = {
    mode: 'development',
    entry: './src/index.tsx',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js',
        publicPath: '/',
    },

    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.json'],
    },

    module: {
        rules: [
            { test: /\.tsx?$/, loader: 'awesome-typescript-loader' },
            { test: /\.s[ac]ss$/i, use: ['style-loader', 'css-loader', 'sass-loader'] },
            // { test: /\.css$/i, use: ['style-loader', 'css-loader'] },
            { test: /\.svg$/i, use: ['@svgr/webpack'] },
        ],
    },
    plugins: [
        htmlPlugin,
        new webpack.EnvironmentPlugin({
            NODE_ENV: 'development',
            API_URL: '/rest/v1/',
        }),
    ],
    devServer: {
        historyApiFallback: true,
        port: process.env.DEVSERVER_PORT || 8081,
    },
};

export default config;
