import * as React from 'react';
import { Provider } from 'react-redux';
import { render } from 'react-dom';

import configureStore from './core/store/configureStore';

import AppContainer from './web/AppContainer';

const store = configureStore();
const rootElement = document.getElementById('root');

export { store };

render(
    <Provider store={store}>
        <AppContainer />
    </Provider>,
    rootElement,
);
