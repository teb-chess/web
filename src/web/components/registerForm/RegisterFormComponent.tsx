import * as React from 'react';
import { ChangeEvent, FormEvent } from 'react';
import {
    Box,
    Button,
    Checkbox,
    createStyles,
    FormControlLabel,
    Link,
    TextField,
    Typography,
    withStyles,
} from '@material-ui/core';
import GoogleIcon from '../../icons/GoogleIcon';
import FacebookIcon from '../../icons/FacebookIcon';
import { Error } from '@material-ui/icons';
import PasswordInput from '../simple/PasswordInput';
import { Link as RouterLink } from 'react-router-dom';

interface Props {
    register(login: string, email: string, password: string, repeatPassword: string): void;
    setLoading(bool: boolean): void;

    token: string;
    isLoading: boolean;
    error: string | Error;
    classes: any;
}

interface State {
    login: string;
    email: string;
    password: string;
    repeatPassword: string;
}

const styles = (theme: { spacing: (...args: any) => any; [key: string]: any }): Record<any, any> =>
    createStyles({
        submit: {
            margin: theme.spacing(3, 0, 3),
        },
        heading: {
            paddingBottom: theme.spacing(1),
        },
        subHeading: {
            paddingBottom: theme.spacing(2),
        },
        signInWith: {
            width: 160,
        },
        divider: {
            display: 'flex',
            alignItems: 'center',
            textAlign: 'center',
            '&:before,&:after': {
                content: "''",
                flex: 1,
                borderBottom: '1px solid ' + theme.palette.text.secondary,
            },
            '&:before': {
                marginRight: '0.5em',
            },
            '&:after': {
                marginLeft: '0.5em',
            },
        },
        input: {
            '&:-webkit-autofill': {
                color: 'green',
            },
        },
    });

class RegisterFormComponent extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            login: '',
            email: '',
            repeatPassword: '',
            password: '',

        };
    }

    public handleForm(e: FormEvent<HTMLFormElement>): void {
        e.preventDefault();
        this.props.register(this.state.login, this.state.email, this.state.password, this.state.repeatPassword);
    }

    public render(): JSX.Element {
        return (
            <>
                <form onSubmit={this.handleForm.bind(this)} style={{ width: 350 }}>
                    <Typography align="center" className={this.props.classes.heading} component="h1" variant="h5">
                        Sign in
                    </Typography>
                    <Box
                        color="error.main"
                        fontSize={14}
                        style={{ visibility: this.props.error ? 'visible' : 'hidden' }}
                    >
                        {this.props.error || 'Error'}
                    </Box>
                    <TextField
                        error={Boolean(this.props.error)}
                        variant="outlined"
                        margin="normal"
                        required
                        InputLabelProps={{ required: false }}
                        fullWidth
                        label="Login"
                        name="text"
                        autoComplete="username"
                        autoFocus
                        className={this.props.classes.input}
                        value={this.state.login}
                        onChange={(e: ChangeEvent<HTMLInputElement>): void => this.setState({ login: e.target.value })}
                    />
                    <TextField
                        error={Boolean(this.props.error)}
                        variant="outlined"
                        margin="normal"
                        required
                        InputLabelProps={{ required: false }}
                        fullWidth
                        label="Email"
                        name="text"
                        autoComplete="email"
                        autoFocus
                        className={this.props.classes.input}
                        value={this.state.email}
                        onChange={(e: ChangeEvent<HTMLInputElement>): void => this.setState({ email: e.target.value })}
                        type="email"
                    />
                    <PasswordInput
                        error={Boolean(this.props.error)}
                        variant="outlined"
                        margin="normal"
                        required
                        InputLabelProps={{ required: false }}
                        fullWidth
                        name="password"
                        label="Password"
                        autoComplete="new-password"
                        className={this.props.classes.input}
                        value={this.state.password}
                        onChange={(e: ChangeEvent<HTMLInputElement>): void =>
                            this.setState({ password: e.target.value })
                        }
                    />
                    <PasswordInput
                        error={Boolean(this.props.error)}
                        variant="outlined"
                        margin="normal"
                        required
                        InputLabelProps={{ required: false }}
                        fullWidth
                        name="password"
                        label="Repeat password"
                        autoComplete="new-password"
                        className={this.props.classes.input}
                        value={this.state.repeatPassword}
                        onChange={(e: ChangeEvent<HTMLInputElement>): void =>
                            this.setState({ repeatPassword: e.target.value })
                        }
                    />
                    <Box
                        display="flex"
                        flexDirection="row"
                        justifyContent="space-between"
                        alignItems="center"
                        className={this.props.classes.submit}
                    >
                        <Link component={RouterLink} to="/login">
                            Sign in
                        </Link>
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            style={{ width: 100 }}
                            disabled={this.props.isLoading}
                        >
                            Register
                        </Button>
                    </Box>
                </form>
            </>
        );
    }
}

export default withStyles(styles)(RegisterFormComponent);
