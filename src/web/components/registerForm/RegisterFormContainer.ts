import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import AuthFormComponent from './RegisterFormComponent';
import {authorize, register} from '../../../core/actionCreators/authActionCreators';
import * as actionTypes from '../../../core/actionTypes/authActionTypes';
import { AuthAction } from '../../../core/actionTypes/authActionTypes';
import * as React from 'react';

const mapStateToProps = (state: any): { isLoading: boolean; error: string | Error } => {
    return {
        isLoading: state.isLoading[actionTypes.REGISTER],
        error: state.error[actionTypes.REGISTER],
    };
};

interface DispatchProps {
    register(login: string, email: string, password: string, repeatPassword: string): void;
}

const mapDispatchToProps = (dispatch: Dispatch<AuthAction>): DispatchProps => ({
    register: (login: string, email: string, password: string, repeatPassword: string): void => {
        dispatch(register(login, email, password, repeatPassword));
    },
});

export default (connect(mapStateToProps, mapDispatchToProps)(AuthFormComponent) as any) as React.ComponentType<{
    setLoading(bool: boolean): void;
}>;
