import * as React from 'react';

interface Props {
    lyrics: string;
    isLoading: boolean;
    error: Error | string;
}

export default class LyricsComponent extends React.Component<Props> {
    render(): JSX.Element {
        const { lyrics, isLoading, error } = this.props;
        if (error) {
            console.log('error: ', error);
            return <div>{error}</div>;
        }

        if (isLoading) {
            return <div>{'Loading..'}</div>;
        }

        return <p style={{ whiteSpace: 'pre-line' }}>{lyrics}</p>;
    }
}
