import { connect } from 'react-redux';
import * as actionTypes from '../../../core/actionTypes/lyricsActionTypes';
import LyricsComponent from './LyricsComponent';

const mapStateToProps = (state: any) => {
    return {
        lyrics: state.lyrics.lyrics,
        isLoading: state.isLoading[actionTypes.GET_LYRICS],
        error: state.error[actionTypes.GET_LYRICS],
    };
};

export default connect(mapStateToProps, null)(LyricsComponent);
