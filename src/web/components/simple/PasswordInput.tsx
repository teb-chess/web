import { TextFieldProps, TextField, InputAdornment, Box } from '@material-ui/core';
import * as React from 'react';
import { RemoveRedEye, Visibility, VisibilityOff } from '@material-ui/icons';

export default class PasswordInput extends React.Component<TextFieldProps, { passwordMasked: boolean }> {
    constructor(props: TextFieldProps) {
        super(props);

        this.state = {
            passwordMasked: true,
        };
    }

    private setPasswordMask(mask = !this.state.passwordMasked): void {
        this.setState({
            passwordMasked: mask,
        });
    }

    public render(): JSX.Element {
        return (
            <TextField
                type={this.state.passwordMasked ? 'password' : 'text'}
                {...this.props}
                InputProps={{
                    endAdornment: (
                        <InputAdornment position="end">
                            <Box
                                style={{ cursor: 'pointer', height: 24 }}
                                onMouseDown={(): void => this.setPasswordMask(false)}
                                onMouseUp={(): void => this.setPasswordMask(true)}
                            >
                                {this.state.passwordMasked ? <VisibilityOff /> : <Visibility />}
                            </Box>
                        </InputAdornment>
                    ),
                }}
            />
        );
    }
}
