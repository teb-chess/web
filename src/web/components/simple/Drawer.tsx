import { createStyles, Divider, Hidden, Theme, Drawer as UIDrawer, withStyles, withTheme } from '@material-ui/core';
import * as React from 'react';

interface Props {
    width?: number;
    classes: any;
    theme: Theme;
    callbacks?: (toggle: () => void) => void;
}

interface State {
    mobileOpen: boolean;
}

const styles = (theme: Theme): Record<any, any> =>
    createStyles({
        drawer: {
            [theme.breakpoints.up('sm')]: {
                flexShrink: 0,
            },
        },
        toolbar: theme.mixins.toolbar,
        drawerPaperPc: {
            height: `calc(100% - ${(theme.mixins.toolbar[theme.breakpoints.up('sm')] as any).minHeight}px)`,
            marginTop: (theme.mixins.toolbar[theme.breakpoints.up('sm')] as any).minHeight,
        },
        drawerPaper: {
            width: 'inherit',
        },
    });

class Drawer extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = {
            mobileOpen: false,
        };

        if (this.props.callbacks) this.props.callbacks(this.toggleOpened.bind(this));
        if (this.props.width) this.width = this.props.width;
    }

    private readonly width: number = 240;

    public toggleOpened(): void {
        this.setState({ mobileOpen: !this.state.mobileOpen });
    }

    public render(): JSX.Element {
        return (
            <nav className={this.props.classes.drawer} aria-label="mailbox folders">
                <Hidden smUp implementation="js">
                    <UIDrawer
                        variant="temporary"
                        anchor={this.props.theme.direction === 'rtl' ? 'right' : 'left'}
                        open={this.state.mobileOpen}
                        onClose={this.toggleOpened.bind(this)}
                        classes={{
                            paper: this.props.classes.drawerPaper,
                        }}
                        style={{
                            width: this.width,
                        }}
                        ModalProps={{
                            keepMounted: true,
                        }}
                    >
                        <div className={this.props.classes.toolbar} />
                        <Divider />
                        {this.props.children}
                    </UIDrawer>
                </Hidden>
                <Hidden xsDown implementation="css">
                    <UIDrawer
                        classes={{
                            paper: `${this.props.classes.drawerPaperPc} ${this.props.classes.drawerPaper}`,
                        }}
                        style={{
                            width: this.width,
                        }}
                        variant="permanent"
                        open
                    >
                        {this.props.children}
                    </UIDrawer>
                </Hidden>
            </nav>
        );
    }
}

export default withTheme(withStyles(styles)(Drawer));
