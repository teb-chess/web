import { connect } from 'react-redux';
import { Dispatch } from 'redux';

import { setLyrics } from '../../../core/actionCreators/lyricsActionCreators';
import { LyricsAction } from '../../../core/actionTypes/lyricsActionTypes';
import { getLyrics } from '../../../core/actionCreators/lyricsActionCreators';
import SearchComponent from './SearchComponent';

const mapDispatchToProps = (dispatch: Dispatch<LyricsAction>) => ({
    onSearch: (artist: string, song: string): void => {
        dispatch(getLyrics(artist, song));
    },
    onClear: (): void => {
        dispatch(setLyrics(''));
    },
});

export default connect(null, mapDispatchToProps)(SearchComponent);
