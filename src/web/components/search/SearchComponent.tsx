import * as React from 'react';

interface Props {
    onSearch(artist: string, song: string): void;

    onClear(): void;
}

interface State {
    artist: string;
    song: string;
}

export default class SearchComponent extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            artist: '',
            song: '',
        };
    }

    public render(): JSX.Element {
        const { onSearch, onClear } = this.props;

        return (
            <div>
                <input
                    value={this.state.artist}
                    placeholder="artist"
                    onChange={(e): void => this.setState({ artist: e.target.value })}
                />
                <input
                    value={this.state.song}
                    placeholder="song"
                    onChange={(e): void => this.setState({ song: e.target.value })}
                />
                <button
                    onClick={(): void => {
                        onSearch(this.state.artist, this.state.song);
                        this.setState({ artist: '', song: '' });
                    }}
                >
                    Get Lyrics
                </button>
                <button onClick={onClear}>Clear Results</button>
            </div>
        );
    }
}
