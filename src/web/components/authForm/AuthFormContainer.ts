import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import AuthFormComponent from './AuthFormComponent';
import { authorize } from '../../../core/actionCreators/authActionCreators';
import * as actionTypes from '../../../core/actionTypes/authActionTypes';
import { AuthAction } from '../../../core/actionTypes/authActionTypes';
import * as React from 'react';

const mapStateToProps = (state: any): { isLoading: boolean; error: string | Error } => {
    return {
        isLoading: state.isLoading[actionTypes.AUTHORIZE],
        error: state.error[actionTypes.AUTHORIZE],
    };
};

interface DispatchProps {
    logIn(login: string, password: string): void;
}

const mapDispatchToProps = (dispatch: Dispatch<AuthAction>): DispatchProps => ({
    logIn: (login: string, password: string, rememberMe?: boolean): void => {
        dispatch(authorize(login, password, rememberMe));
    },
});

export default (connect(mapStateToProps, mapDispatchToProps)(AuthFormComponent) as any) as React.ComponentType<{
    setLoading(bool: boolean): void;
}>;
