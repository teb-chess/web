import * as React from 'react';
import { ChangeEvent, FormEvent } from 'react';
import {
    Box,
    Button,
    Checkbox,
    createStyles,
    FormControlLabel,
    Link,
    TextField,
    Typography,
    withStyles,
} from '@material-ui/core';
import GoogleIcon from '../../icons/GoogleIcon';
import FacebookIcon from '../../icons/FacebookIcon';
import { Error } from '@material-ui/icons';
import PasswordInput from '../simple/PasswordInput';
import { Link as RouterLink } from 'react-router-dom';

interface Props {
    logIn(login: string, password: string, rememberMe?: boolean): void;

    setLoading(bool: boolean): void;

    rememberMe(remember: boolean): void;

    token: string;
    isLoading: boolean;
    error: string | Error;
    classes: any;
}

interface State {
    login: string;
    password: string;
    rememberMe: boolean;
}

const styles = (theme: { spacing: (...args: any) => any; [key: string]: any }): Record<any, any> =>
    createStyles({
        submit: {
            margin: theme.spacing(3, 0, 3),
        },
        heading: {
            paddingBottom: theme.spacing(1),
        },
        subHeading: {
            paddingBottom: theme.spacing(2),
        },
        signInWith: {
            width: 160,
        },
        divider: {
            display: 'flex',
            alignItems: 'center',
            textAlign: 'center',
            '&:before,&:after': {
                content: "''",
                flex: 1,
                borderBottom: '1px solid ' + theme.palette.text.secondary,
            },
            '&:before': {
                marginRight: '0.5em',
            },
            '&:after': {
                marginLeft: '0.5em',
            },
        },
        input: {
            '&:-webkit-autofill': {
                color: 'green',
            },
        },
    });

class AuthFormComponent extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            login: '',
            password: '',
            rememberMe: false,
        };
    }

    public handleForm(e: FormEvent<HTMLFormElement>): void {
        e.preventDefault();
        this.props.logIn(this.state.login, this.state.password, this.state.rememberMe);
    }

    public render(): JSX.Element {
        return (
            <>
                <form onSubmit={this.handleForm.bind(this)} style={{ width: 350 }}>
                    <Typography align="center" className={this.props.classes.heading} component="h1" variant="h5">
                        Sign in
                    </Typography>
                    <Box
                        color="error.main"
                        fontSize={14}
                        style={{ visibility: this.props.error ? 'visible' : 'hidden' }}
                    >
                        {this.props.error || 'Error'}
                    </Box>
                    <TextField
                        error={Boolean(this.props.error)}
                        variant="outlined"
                        margin="normal"
                        required
                        InputLabelProps={{ required: false }}
                        fullWidth
                        label="Login"
                        name="text"
                        autoComplete="username"
                        autoFocus
                        className={this.props.classes.input}
                        value={this.state.login}
                        onChange={(e: ChangeEvent<HTMLInputElement>): void => this.setState({ login: e.target.value })}
                    />
                    <PasswordInput
                        error={Boolean(this.props.error)}
                        variant="outlined"
                        margin="normal"
                        required
                        InputLabelProps={{ required: false }}
                        fullWidth
                        name="password"
                        label="Password"
                        autoComplete="password"
                        className={this.props.classes.input}
                        value={this.state.password}
                        onChange={(e: ChangeEvent<HTMLInputElement>): void =>
                            this.setState({ password: e.target.value })
                        }
                    />
                    <FormControlLabel
                        control={
                            <Checkbox
                                value="remember"
                                color="primary"
                                checked={this.state.rememberMe}
                                onChange={(e: ChangeEvent<HTMLInputElement>): void =>
                                    this.setState({ rememberMe: e.target.checked })
                                }
                            />
                        }
                        label="Remember me"
                    />
                    <br />
                    <Box
                        display="flex"
                        flexDirection="row"
                        justifyContent="space-between"
                        alignItems="center"
                        className={this.props.classes.submit}
                    >
                        <Link component={RouterLink} to="/login/register">
                            Register
                        </Link>
                        <Link component={RouterLink} to="/login/restore_password">
                            Forgot password?
                        </Link>
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            style={{ width: 100 }}
                            disabled={this.props.isLoading}
                        >
                            Sign in
                        </Button>
                    </Box>
                    <Typography align="center" className={this.props.classes.divider}>
                        Or sign in with:
                    </Typography>
                    <br />
                    <Box display="flex" flexDirection="row" justifyContent="space-between">
                        <Button variant="outlined" startIcon={<GoogleIcon />} className={this.props.classes.signInWith}>
                            {' '}
                            Google
                        </Button>
                        <Button
                            variant="outlined"
                            startIcon={<FacebookIcon />}
                            className={this.props.classes.signInWith}
                        >
                            {' '}
                            Facebook
                        </Button>
                    </Box>
                </form>
            </>
        );
    }
}

export default withStyles(styles)(AuthFormComponent);
