import * as React from 'react';
import {Route, Router, Switch} from 'react-router';
import LoginContainer from './pages/auth/AuthContainer';
import {BrowserRouter, HashRouter} from 'react-router-dom';
import {createMuiTheme, ThemeProvider} from '@material-ui/core/styles';
import {CssBaseline} from '@material-ui/core';
import {blue} from '@material-ui/core/colors';
import TestContainer from './pages/index/IndexContainer';
import {getPreferredTheme} from '../core/store/theme';
import {history} from "../core/store/configureStore";

export interface AppProps {
    theme: string;
}

class App extends React.Component<AppProps> {
    constructor(props: AppProps) {
        super(props);
    }

    private static routes(): JSX.Element {
        return (
            <Switch>
                <Route path="/login" component={LoginContainer} />
                {/*<Route path="/login/restore_password" component={LoginContainer} />*/}
                <Route path="/" component={TestContainer} />
            </Switch>
        );
    }

    public router(): JSX.Element {
        return <Router history={history}>{App.routes()}</Router>;
    }

    public render(): JSX.Element {
        const theme = createMuiTheme({
            palette: {
                type: this.props.theme === 'auto' ? getPreferredTheme() : (this.props.theme as any),
                primary: blue,
            },
            overrides: {
                MuiCssBaseline: {
                    '@global': {
                        '*::-webkit-scrollbar': {
                            width: '0.4em',
                        },
                        '*::-webkit-scrollbar-track': {
                            '-webkit-box-shadow': 'inset 0 0 6px rgba(0,0,0,0.00)',
                        },
                        '*::-webkit-scrollbar-thumb': {
                            backgroundColor: this.props.theme === 'light' ? 'rgba(0,0,0,.2)' : 'rgba(255,255,255,.2)',
                            outline: '1px solid slategrey',
                        },
                    },
                },
            },
        });

        return (
            <ThemeProvider theme={theme}>
                <CssBaseline />
                {this.router()}
            </ThemeProvider>
        );
    }
}

export default App;
