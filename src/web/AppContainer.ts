import { connect } from 'react-redux';
import App, { AppProps } from './App';
import { AppState } from '../core/reducers/rootReducer';
import * as React from 'react';

const mapStateToProps = (state: AppState): AppProps => {
    return {
        theme: state.theme.theme,
    };
};

export default (connect(mapStateToProps, null)(App) as any) as React.ComponentType;
