import { connect } from 'react-redux';
import IndexPage from './IndexPage';
import { Dispatch } from 'redux';
import { logOut } from '../../../core/actionCreators/authActionCreators';
import { AuthAction } from '../../../core/actionTypes/authActionTypes';
import { getTokenData, JWT, JWTAction } from '../../../core/services/Token';

const mapStateToProps = (state: any): { token: string; userInfo: JWT | JWTAction } => {
    return {
        token: state.auth.token,
        userInfo: getTokenData(state.auth.token),
    };
};

const mapDispatchToProps = (dispatch: Dispatch<AuthAction>): { logOut: () => void } => ({
    logOut(): void {
        dispatch(logOut());
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(IndexPage);
