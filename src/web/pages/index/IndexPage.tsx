import * as React from 'react';
import {
    AppBar,
    Backdrop,
    Button,
    CircularProgress,
    createStyles,
    Divider,
    IconButton,
    List,
    ListItem,
    ListItemIcon,
    ListItemText,
    Theme,
    Toolbar,
    Typography,
    withStyles,
    withTheme,
} from '@material-ui/core';
import { Redirect, Route, Switch as RouterSwitch } from 'react-router';
import { JWT, JWTAction } from '../../../core/services/Token';
import {
    Assignment as AssignmentIcon,
    Inbox as InboxIcon,
    Mail as MailIcon,
    Menu as MenuIcon,
} from '@material-ui/icons';
import Drawer from '../../components/simple/Drawer';

interface Props {
    logOut(): void;

    token: string;
    userInfo: JWT | JWTAction;
    classes: Record<any, any>;
    theme: Theme;
}

interface State {
    mobileOpen: boolean;
}

const styles = (theme: Theme): Record<any, any> =>
    createStyles({
        title: {
            flexGrow: 1,
        },
        appBar: {
            zIndex: theme.zIndex.drawer + 1,
            background: theme.palette.background.paper,
            borderBottom: `solid 1px ${theme.palette.divider}`,
        },
        content: {
            flexGrow: 1,
            display: 'flex',
            flexDirection: 'column',
            height: 'inherit',
        },
        menuButton: {
            marginRight: theme.spacing(2),
            [theme.breakpoints.up('sm')]: {
                display: 'none',
            },
        },
        toolbar: {
            ...theme.mixins.toolbar,
            flex: '0 1 auto',
        },
        root: {
            display: 'flex',
            height: 'inherit',
        },
        main: {
            flex: '1 1 auto',
            padding: theme.spacing(3),
            overflowY: 'auto',
        },
    });

class IndexPage extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
    }

    public toggleMobileDrawer: () => void = () => {
        /**/
    };

    public render(): JSX.Element {
        if (!this.props.token) {
            return <Redirect to="/login" />;
        }

        return <>authorized!</>;

        if (this.props.userInfo.info !== 'data') {
            return (
                <Backdrop open>
                    <CircularProgress color="inherit" />
                </Backdrop>
            );
        }

        return (
            <div className={this.props.classes.root}>
                <AppBar position="fixed" color="transparent" className={this.props.classes.appBar} elevation={0}>
                    <Toolbar>
                        <IconButton
                            edge="start"
                            color="inherit"
                            aria-label="menu"
                            className={this.props.classes.menuButton}
                            onClick={(): void => this.toggleMobileDrawer()}
                        >
                            <MenuIcon />
                        </IconButton>
                        <Typography variant="h6" className={this.props.classes.title}>
                            Chess
                        </Typography>
                        {/*<Button color="inherit">Login</Button>*/}
                    </Toolbar>
                </AppBar>
                <Drawer
                    callbacks={(toggle): any => {
                        this.toggleMobileDrawer = toggle;
                    }}
                >
                    <div>
                        <List>
                            <ListItem button>
                                <ListItemIcon>
                                    <AssignmentIcon />
                                </ListItemIcon>
                                <ListItemText primary="Grades" />
                            </ListItem>
                        </List>
                        {/*<List>*/}
                        {/*    {['Inbox', 'Starred', 'Send email', 'Drafts'].map((text, index) => (*/}
                        {/*        <ListItem button key={text}>*/}
                        {/*            <ListItemIcon>{index % 2 === 0 ? <InboxIcon /> : <MailIcon />}</ListItemIcon>*/}
                        {/*            <ListItemText primary={text} />*/}
                        {/*        </ListItem>*/}
                        {/*    ))}*/}
                        {/*</List>*/}
                        {/*<Divider />*/}
                        {/*<List>*/}
                        {/*    {['All mail', 'Trash', 'Spam'].map((text, index) => (*/}
                        {/*        <ListItem button key={text}>*/}
                        {/*            <ListItemIcon>{index % 2 === 0 ? <InboxIcon /> : <MailIcon />}</ListItemIcon>*/}
                        {/*            <ListItemText primary={text} />*/}
                        {/*        </ListItem>*/}
                        {/*    ))}*/}
                        {/*</List>*/}
                    </div>
                </Drawer>
                <div className={this.props.classes.content}>
                    <div className={this.props.classes.toolbar} />
                    <main className={this.props.classes.main}>
                        <RouterSwitch>
                            <Route
                                exact
                                path="/"
                                render={(): any => (
                                    <>
                                        Ахуеть блять
                                        <br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a
                                        <br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a
                                        <br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a
                                        <br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a
                                        <br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a
                                        <br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a
                                        <br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a
                                        <br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a
                                        <br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a
                                        <br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a
                                        <br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a
                                        <br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a
                                        <br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a
                                        <br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a
                                        <br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a
                                        <br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a
                                        <br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a
                                        <br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a
                                        <br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a
                                        <br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a
                                        <br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a<br />a
                                    </>
                                )}
                            />
                        </RouterSwitch>
                    </main>
                </div>
            </div>
        );
    }
}

export default withTheme(withStyles(styles)(IndexPage));
