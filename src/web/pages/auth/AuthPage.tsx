import * as React from 'react';
import AuthFormContainer from '../../components/authForm/AuthFormContainer';
import {
    Box,
    createStyles,
    FormControlLabel,
    Grid,
    LinearProgress,
    Link,
    MenuItem,
    Paper,
    Select,
    Switch,
    withStyles,
} from '@material-ui/core';
import { Theme } from '../../../core/actionTypes/themeActionTypes';
import { ChangeEvent } from 'react';
import { Redirect, Route, Switch as RouterSwitch } from 'react-router';
import { Link as RouterLink } from 'react-router-dom';
import RegisterFormContainer from "../../components/registerForm/RegisterFormContainer";

interface Props {
    setTheme(theme: Theme): void;
    theme: string;
    classes: Record<any, any>;
    token: string;
    isLoading: boolean;
}

interface State {
    loading: boolean;
}

const styles = (theme: { spacing: (...args: any) => any }): Record<any, any> =>
    createStyles({
        grid: {
            height: '100%',
        },
        scrollable: {
            overflowY: 'auto',
        },
        paper: {
            padding: theme.spacing(8, 5),
        },
        bottomButtons: {
            paddingTop: theme.spacing(1),
        },
        select: {
            marginTop: 6,
            marginLeft: 12,
        },
        selectRight: {
            marginTop: 6,
            marginRight: 12,
        },
        switch: {
            marginRight: 0,
        },
        progress: {
            position: 'relative',
            width: `calc(100% + ${theme.spacing(5)}px + ${theme.spacing(5)}px)`,
            right: theme.spacing(5),
            bottom: theme.spacing(8) + 1,
        },
    });

class AuthPage extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = {
            loading: false,
        };
    }

    private handleThemeChange(e: ChangeEvent<any>): any {
        // this.props.setTheme(e.target.checked ? 'dark' : 'light');
        if (['dark', 'light', 'auto'].includes(e.target.value)) this.props.setTheme(e.target.value);
        else this.props.setTheme('auto');
    }

    public setLoading(bool: boolean): void {
        this.setState({ loading: bool });
    }

    public render(): JSX.Element {
        if (this.props.token) {
            return <Redirect to="/" />;
        }
        return (
            <Grid container className={this.props.classes.grid}>
                <Box
                    display="flex"
                    flexDirection="column"
                    justifyContent="center"
                    alignItems="center"
                    width="100%"
                    height="100%"
                >
                    <Box className={this.props.classes.scrollable}>
                        <Paper className={this.props.classes.paper} variant="outlined">
                            <LinearProgress
                                className={this.props.classes.progress}
                                variant="query"
                                style={{ visibility: this.props.isLoading ? 'visible' : 'hidden' }}
                            />
                            <RouterSwitch>
                                <Route exact path="/login" component={AuthFormContainer} />
                                <Route exact path="/login/register" component={RegisterFormContainer} />
                                <Route
                                    exact
                                    path="/login/restore_password"
                                    render={() => (
                                        <Link component={RouterLink} to="/login">
                                            Oh shit go back
                                        </Link>
                                    )}
                                />
                            </RouterSwitch>
                        </Paper>
                        <Box
                            display="flex"
                            flexDirection="row"
                            justifyContent="space-between"
                            className={this.props.classes.bottomButtons}
                        >
                            <Box>
                                <Select disableUnderline defaultValue="en" className={this.props.classes.select}>
                                    <MenuItem value="en">English</MenuItem>
                                    <MenuItem value="pl">Polish</MenuItem>
                                </Select>
                            </Box>
                            <Box>
                                <Select
                                    disableUnderline
                                    value={this.props.theme}
                                    className={this.props.classes.selectRight}
                                    onChange={this.handleThemeChange.bind(this)}
                                >
                                    <MenuItem value="dark">Dark theme</MenuItem>
                                    <MenuItem value="light">Light theme</MenuItem>
                                    <MenuItem value="auto">Auto theme</MenuItem>
                                </Select>
                            </Box>
                        </Box>
                    </Box>
                </Box>
            </Grid>
        );
    }
}

export default withStyles(styles)(AuthPage);
