import { connect } from 'react-redux';
import AuthPage from './AuthPage';
import { Dispatch } from 'redux';
import { setTheme } from '../../../core/actionCreators/themeActionCreators';
import { SetThemeAction } from '../../../core/actionTypes/themeActionTypes';
import * as actionTypes from '../../../core/actionTypes/authActionTypes';

const mapStateToProps = (state: any): { theme: string; token: string; isLoading: boolean } => {
    return {
        theme: state.theme.theme,
        token: state.auth.token,
        isLoading: state.isLoading[actionTypes.AUTHORIZE],
    };
};

const mapDispatchToProps = (dispatch: Dispatch<SetThemeAction>): { setTheme: (theme: string) => void } => ({
    setTheme(theme: string): void {
        dispatch(setTheme(theme));
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(AuthPage);
