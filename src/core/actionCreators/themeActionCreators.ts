import * as actions from '../actionTypes/themeActionTypes';
import { resolveTheme } from '../store/theme';

export function setTheme(theme: any): actions.SetThemeAction {
    return {
        type: actions.SET_THEME,
        theme: resolveTheme(theme),
    };
}