import * as actions from '../actionTypes/authActionTypes';

export function setToken(token: string): actions.SetTokenAction {
    return {
        type: actions.SET_TOKEN,
        token,
    };
}
export function logOut(): actions.SetTokenAction {
    return {
        type: actions.SET_TOKEN,
        token: '',
    };
}

export function refreshToken(): actions.RefreshTokenAction {
    return {
        type: actions.REFRESH_TOKEN,
    };
}

export function authorize(login: string, password: string, rememberMe = false): actions.AuthorizeAction {
    return {
        type: actions.AUTHORIZE,
        login,
        password,
        rememberMe,
    };
}

export function register(login: string, email: string, password: string, repeatPassword: string): actions.RegisterAction {
    return {
        type: actions.REGISTER,
        login,
        email,
        password,
        repeatPassword
    };
}

export function authorizeRequest(): actions.AuthorizeRequestAction {
    return {
        type: actions.AUTHORIZE_REQUEST,
    };
}

export function authorizeSuccess(token: string): actions.AuthorizeSuccessAction {
    return {
        type: actions.AUTHORIZE_SUCCESS,
        token,
    };
}

export function authorizeFailure(error: Error | string): actions.AuthorizeFailureAction {
    return {
        type: actions.AUTHORIZE_FAILURE,
        error,
    };
}

export function registerRequest(): actions.RegisterRequestAction {
    return {
        type: actions.REGISTER_REQUEST,
    };
}

export function registerSuccess(token: string): actions.RegisterSuccessAction {
    return {
        type: actions.REGISTER_SUCCESS,
        token,
    };
}

export function registerFailure(error: Error | string): actions.RegisterFailureAction {
    return {
        type: actions.REGISTER_FAILURE,
        error,
    };
}
