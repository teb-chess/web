export type Locale = 'ru_RU' | 'en_EN';

export const SET_LOCALE = 'themeActionTypes/SET_LOCALE';
export interface SetLocaleAction {
    type: typeof SET_LOCALE;
    locale: Locale;
}

export type LocaleAction = SetLocaleAction;
