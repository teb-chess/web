export const SET_TOKEN = 'loginActionTypes/SET_TOKEN';
export interface SetTokenAction {
    type: typeof SET_TOKEN;
    token: string;
}

export const REFRESH_TOKEN = 'loginActionTypes/REFRESH_TOKEN';
export interface RefreshTokenAction {
    type: typeof REFRESH_TOKEN;
}

export const AUTHORIZE = 'loginActionTypes/AUTHORIZE';
export interface AuthorizeAction {
    type: typeof AUTHORIZE;
    login: string;
    password: string;
    rememberMe: boolean;
}

export const REGISTER = 'loginActionTypes/REGISTER';
export interface RegisterAction {
    type: typeof REGISTER;
    login: string;
    email: string;
    password: string;
    repeatPassword: string;
}

export const AUTHORIZE_REQUEST = 'loginActionTypes/AUTHORIZE_REQUEST';
export interface AuthorizeRequestAction {
    type: typeof AUTHORIZE_REQUEST;
}

export const AUTHORIZE_SUCCESS = 'loginActionTypes/AUTHORIZE_SUCCESS';
export interface AuthorizeSuccessAction {
    type: typeof AUTHORIZE_SUCCESS;
    token: string;
}

export const AUTHORIZE_FAILURE = 'loginActionTypes/AUTHORIZE_FAILURE';
export interface AuthorizeFailureAction {
    type: typeof AUTHORIZE_FAILURE;
    error: Error | string;
}

export const REGISTER_REQUEST = 'loginActionTypes/REGISTER_REQUEST';
export interface RegisterRequestAction {
    type: typeof REGISTER_REQUEST;
}

export const REGISTER_SUCCESS = 'loginActionTypes/REGISTER_SUCCESS';
export interface RegisterSuccessAction {
    type: typeof REGISTER_SUCCESS;
    token: string;
}

export const REGISTER_FAILURE = 'loginActionTypes/REGISTER_FAILURE';
export interface RegisterFailureAction {
    type: typeof REGISTER_FAILURE;
    error: Error | string;
}

export type AuthAction =
    | SetTokenAction
    | AuthorizeAction
    | AuthorizeRequestAction
    | AuthorizeSuccessAction
    | AuthorizeFailureAction
    | RegisterAction
    | RegisterRequestAction
    | RegisterSuccessAction
    | RegisterFailureAction;
