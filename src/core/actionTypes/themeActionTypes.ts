export type Theme = 'dark' | 'light' | 'auto';
export const ThemeList = ['dark', 'light', 'auto'];

export const SET_THEME = 'themeActionTypes/SET_THEME';
export interface SetThemeAction {
    type: typeof SET_THEME;
    theme: Theme;
}

export type ThemeAction = SetThemeAction;
