import { combineReducers } from 'redux';
import lyricsReducer from './lyricsReducer';
import isLoadingReducer from './isLoadingReducer';
import errorReducer from './errorReducer';
import authReducer from './authReducer';
import themeReducer from './themeReducer';

const rootReducer = combineReducers({
    lyrics: lyricsReducer,
    auth: authReducer,
    isLoading: isLoadingReducer,
    error: errorReducer,
    theme: themeReducer,
});

export type AppState = ReturnType<typeof rootReducer>;

export default rootReducer;
