import * as actions from '../actionTypes/themeActionTypes';
import { resolveTheme } from '../store/theme';

export interface ThemeState {
    theme: string;
}

const initialState: ThemeState = {
    theme: resolveTheme(localStorage.getItem('theme')),
};

export default function themeReducer(state: ThemeState = initialState, action: actions.ThemeAction): ThemeState {
    // noinspection JSRedundantSwitchStatement todo remove this
    switch (action.type) {
        case actions.SET_THEME:
            return {
                theme: action.theme,
            };
        default:
            return state;
    }
}
