import * as actions from '../actionTypes/authActionTypes';
import {history} from "../store/configureStore";

export interface AuthState {
    token: string;
}

const initialState: AuthState = {
    token: localStorage.getItem('token') || '',
};

export default function authReducer(state: AuthState = initialState, action: actions.AuthAction): AuthState {
    switch (action.type) {
        case actions.SET_TOKEN:
        case actions.AUTHORIZE_SUCCESS:
            return {
                ...state,
                token: action.token,
            };
        case actions.REGISTER_SUCCESS:
            location.href = '/login';
            return state;
        default:
            return state;
    }
}
