import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import * as actionCreators from '../actionCreators/authActionCreators';
import * as actionTypes from '../actionTypes/authActionTypes';
import API from '../services/API';
import {registerFailure} from "../actionCreators/authActionCreators";

namespace AuthSaga {
    export function* onAuthorize({ login, password, rememberMe }: actionTypes.AuthorizeAction): any {
        try {
            yield put(actionCreators.authorizeRequest());
            const token = yield call(API.authorize, login, password, rememberMe);
            yield put(actionCreators.authorizeSuccess(token));
        } catch (error) {
            yield put(actionCreators.authorizeFailure(error.message));
        }
    }

    export function* onRegister({ login, email, password, repeatPassword }: actionTypes.RegisterAction): any {
        try {
            yield put(actionCreators.registerRequest());
            if (password !== repeatPassword) {
                yield put(registerFailure('Passwords didn\'t match'));
                return;
            }
            const token = yield call(API.register, login, email, password);
            yield put(actionCreators.registerSuccess(token));
        } catch (error) {
            yield put(actionCreators.registerFailure(error.message));
        }
    }

    export function* onRefresh(): any {
        try {
            const token = yield call(API.refreshToken);
            yield put(actionCreators.setToken(token || ''));
        } catch (error) {
            yield put(actionCreators.logOut());
        }
    }

    export function* watchOnLoadLyrics(): any {
        yield takeEvery(actionTypes.AUTHORIZE, AuthSaga.onAuthorize);
        yield takeEvery(actionTypes.REGISTER, AuthSaga.onRegister);
        yield takeEvery(actionTypes.REFRESH_TOKEN, AuthSaga.onRefresh);
    }

    export function* saga(): any {
        yield all([fork(AuthSaga.watchOnLoadLyrics)]);
    }
}

export default AuthSaga;
