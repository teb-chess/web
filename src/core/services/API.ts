import axios, { Method, AxiosResponse, AxiosError } from 'axios';
import { Utils } from '../Utils';

namespace API {
    import mergeDeep = Utils.mergeDeep;

    export const statusCodes: {[key: string]: string} = {
        '100': 'Continue',
        '101': 'Switching Protocols',
        '102': 'Processing',
        '200': 'OK',
        '201': 'Created',
        '202': 'Accepted',
        '203': 'Non-Authoritative Information',
        '204': 'No Content',
        '205': 'Reset Content',
        '206': 'Partial Content',
        '207': 'Multi-Status',
        '300': 'Multiple Choices',
        '301': 'Moved Permanently',
        '302': 'Moved Temporarily',
        '303': 'See Other',
        '304': 'Not Modified',
        '305': 'Use Proxy',
        '307': 'Temporary Redirect',
        '400': 'Bad Request',
        '401': 'Unauthorized',
        '402': 'Payment Required',
        '403': 'Forbidden',
        '404': 'Not Found',
        '405': 'Method Not Allowed',
        '406': 'Not Acceptable',
        '407': 'Proxy Authentication Required',
        '408': 'Request Time-out',
        '409': 'Conflict',
        '410': 'Gone',
        '411': 'Length Required',
        '412': 'Precondition Failed',
        '413': 'Request Entity Too Large',
        '414': 'Request-URI Too Large',
        '415': 'Unsupported Media Type',
        '416': 'Requested Range Not Satisfiable',
        '417': 'Expectation Failed',
        '418': "I'm a teapot",
        '422': 'Unprocessable Entity',
        '423': 'Locked',
        '424': 'Failed Dependency',
        '425': 'Unordered Collection',
        '426': 'Upgrade Required',
        '428': 'Precondition Required',
        '429': 'Too Many Requests',
        '431': 'Request Header Fields Too Large',
        '451': 'Unavailable For Legal Reasons',
        '500': 'Internal Server Error',
        '501': 'Not Implemented',
        '502': 'Bad Gateway',
        '503': 'Service Unavailable',
        '504': 'Gateway Time-out',
        '505': 'HTTP Version Not Supported',
        '506': 'Variant Also Negotiates',
        '507': 'Insufficient Storage',
        '509': 'Bandwidth Limit Exceeded',
        '510': 'Not Extended',
        '511': 'Network Authentication Required',
    };

    export async function request(endpoint: string, method: Method = 'get', options: any = {}): Promise<AxiosResponse> {
        let headers = {};
        const token = localStorage.getItem('token');
        if (token) headers = { Authorization: `Bearer ${token}` };
        return axios(
            mergeDeep(
                {
                    baseURL: process.env.API_URL,
                    url: endpoint,
                    method,
                    headers,
                    transformResponse: [(data: any): any => JSON.parse(data)],
                },
                options,
            ),
        );
    }

    export function handleError(e: AxiosError): void {
        if (!e.response || !e.response.data) throw new Error('Cannot connect to the server');
        if (!e.response.data || !(e.response.status in API.statusCodes)) e.response.status = 500;
        if (e.response.data.message) {
            // noinspection JSRedundantSwitchStatement todo remove noinspection
            switch (e.response.data.message) {
                case 'invalid_credentials':
                    throw new Error('Invalid login or password');
            }
            throw new Error(`${API.statusCodes[e.response.status.toString()]} (${e.response.data.message})`);
        }
        throw new Error(API.statusCodes[e.response.status.toString()]);
    }

    export async function authorize(login: string, password: string, rememberMe: boolean): Promise<string | undefined> {
        try {
            const request = await API.request('/auth', 'post', {
                data: {
                    login,
                    password,
                    rememberMe,
                },
            });
            return request.data.token;
        } catch (e) {
            API.handleError(e);
        }
    }

    export async function register(login: string, email: string, password: string): Promise<boolean> {
        try {
            const request = await API.request('/auth/register', 'post', {
                data: {
                    login,
                    email,
                    password,
                },
            });
            return request.data.ok ?? false;
        } catch (e) {
            API.handleError(e);
        }

        return false;
    }

    export async function refreshToken(): Promise<string | undefined> {
        try {
            const request = await API.request('/auth/refresh', 'post');
            return request.data.token;
        } catch (e) {
            API.handleError(e);
        }
    }
}

export default API;
