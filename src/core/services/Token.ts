import { decode } from 'jsonwebtoken';
import { store } from '../../index';
import { logOut, refreshToken } from '../actionCreators/authActionCreators';


export type JWTRaw = {
    sub: string; // user id
    iss: 'chess'; // issuer
    exp: number; // expires at
    jti: string; // token id
    rmb: boolean; // remember me? (refresh or not)
    rtm: number; // refresh timeout
    rid: string; // refresh id (used to revoke tokens)
};

export type JWT = {
    raw: JWTRaw;
    id: string;
    tokenID: string;
    refreshID: string;
    rememberMe: boolean;
    refreshTimeout: number;
    expires: Date;
    info: 'data';
};

export type JWTAction = {
    info: 'refresh' | 'logout';
};

export function getTokenData(token: string): JWT | JWTAction {
    if (!token) return { info: 'logout' };

    const info: JWTRaw = decode(token) as JWTRaw;
    if (
        !(
            info &&
            'sub' in info &&
            'iss' in info &&
            info.iss == 'chess' &&
            'exp' in info &&
            'jti' in info &&
            'rmb' in info &&
            'rid' in info &&
            'rtm' in info &&
            info.exp + info.rtm > Date.now() / 1000
        )
    ) {
        store.dispatch(logOut());
        return { info: 'logout' };
    } else if (info.exp < Date.now() / 1000) {
        if (info.rmb) {
            store.dispatch(refreshToken());
            return { info: 'refresh' };
        } else {
            store.dispatch(logOut());
            return { info: 'logout' };
        }
    }

    return {
        raw: info,
        id: info.sub,
        tokenID: info.jti,
        refreshID: info.rid,
        rememberMe: info.rmb,
        refreshTimeout: info.rtm,
        expires: new Date(info.exp * 1000),
        info: 'data',
    };
}
