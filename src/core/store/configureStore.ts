import {createBrowserHistory, createHashHistory} from 'history';
import { createStore, applyMiddleware, Store } from 'redux';
import createSagaMiddleware from 'redux-saga';
import logger from 'redux-logger';

import rootReducer from '../reducers/rootReducer';
import rootSaga from '../sagas/rootSaga';
import { setTheme } from '../actionCreators/themeActionCreators';
import { setToken } from '../actionCreators/authActionCreators';

const sagaMiddleware = createSagaMiddleware();

const configureStore = (): Store => {
    const store = createStore(rootReducer, applyMiddleware(sagaMiddleware, logger));
    store.subscribe(() => {
        localStorage.setItem('token', store.getState().auth.token);
        localStorage.setItem('theme', store.getState().theme.theme);
    });
    window.addEventListener('storage', (event: StorageEvent) => {
        if (event.storageArea === localStorage) {
            switch (event.key) {
                case 'theme':
                    store.dispatch(setTheme(event.newValue));
                    break;
                case 'token':
                    store.dispatch(setToken(event.newValue || ''));
                    break;
            }
        }
    });
    sagaMiddleware.run(rootSaga);
    return store;
};

export default configureStore;

const history = location.pathname.endsWith('.html') ? createHashHistory() : createBrowserHistory();

export { history };
