import { Theme, ThemeList } from '../actionTypes/themeActionTypes';
import get = Reflect.get;

export function getPreferredTheme(): Theme {
    let theme = 'light';
    if (window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches) {
        theme = 'dark';
    }
    return theme as Theme;
}

console.log(getPreferredTheme());

export function resolveTheme(theme: any): Theme {
    if (ThemeList.includes((theme || '').toString().toLowerCase())) {
        return theme as Theme;
    } else {
        return getPreferredTheme();
    }
}